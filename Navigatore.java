public class Navigatore {
  
  double latitude;
  double longitude;
  
    public Navigatore (double latitude, double longitude){
      this.latitude = latitude;
      this.longitude = longitude;
    
        
    }

    public String trovaCittaPiuVicina() {
      // I dati delle citta
      String[] cittaNomi = {"Roma", "Bolzano", "Milano", "Firenze"};
      double[] latitudini = {41.9028, 46.4983, 45.4642, 43.7696};
      double[] longitudini = {12.4964, 11.3545, 9.1900, 11.2558};

      // La tua posizione
      double tuaLatitudine = this.latitude; /* Inserisci la tua latitudine 51.507351*/; 
      double tuaLongitudine = this.longitude;/* Inserisci la tua longitudine -0.127758*/;

    
        double distanzaMinima = Double.MAX_VALUE;
        String cittaPiuVicina = "";
         double R = 6371; 
        for (int i = 0; i < cittaNomi.length; i++) {
          double lat1Rad = Math.toRadians(tuaLatitudine);
          double lon1Rad = Math.toRadians(tuaLongitudine);
          double lat2Rad = Math.toRadians(latitudini[i]);
          double lon2Rad = Math.toRadians(longitudini[i]);

          double deltaLat = lat2Rad - lat1Rad;
          double deltaLon = lon2Rad - lon1Rad;

          double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2) + Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
          double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
          
            double distanza = R * c;
            if (distanza < distanzaMinima) {
                distanzaMinima = distanza;
                cittaPiuVicina = cittaNomi[i];
            }
        }

        return "Il nostro punto vendita più vicino si trova a: " + cittaPiuVicina;
    }
}

//     public double calcolaDistanza(double lat1, double lon1, double lat2, double lon2) {
//         double R = 6371; // Raggio della Terra in chilometri
//         double lat1Rad = Math.toRadians(lat1);
//         double lon1Rad = Math.toRadians(lon1);
//         double lat2Rad = Math.toRadians(lat2);
//         double lon2Rad = Math.toRadians(lon2);

//         double deltaLat = lat2Rad - lat1Rad;
//         double deltaLon = lon2Rad - lon1Rad;

//         double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2) + Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
//         double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

//         return R * c;
//     }
// }
